/*
Camera class
- creation of a reference system for the camera that look at determined target position (object)
- the camera rotate around the target on x and z axis, so I rename them as pitch, roll
- only movement is forward or backward, (look closer or farther)

 adaptation of camera.h
*/

#pragma once

// we use GLM to create the view matrix and to manage camera transformations
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// possible camera movements
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// Default camera settings
// Initial camera orientation on Y and Z (X not considered)
const GLfloat ROLL     =  40.0f; //Z
const GLfloat YAW      =  0.0f; //Y

// parameters to manage mouse movement
const GLfloat SPEED      =  10.0f;
const GLfloat SENSITIVITY =  0.05f;

const GLfloat FOLLOW_SPEED = 1.0f;

///////////////////  CAMERA class ///////////////////////
class Camera
{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Target;
    //direction camera to target
    glm::vec3 Front;
    glm::vec3 WorldUp; //  camera world UP vector -> needed for the initial computation of Right vector
    
    //distace between target-camera projected on XZ plane 
    GLfloat DistanceXZ;
    // Eular Angles
    GLfloat Roll;
    GLfloat Yaw;
    // Camera options
    GLfloat MovementSpeed;
    GLfloat MouseSensitivity;

    Camera(glm::vec3 position, glm::vec3 target=glm::vec3(0.0f))
        :Position(position),Target(target),Yaw(YAW),Roll(ROLL),MovementSpeed(SPEED),MouseSensitivity(SENSITIVITY)
    {
        this->DistanceXZ=DistanceV2(this->Position,this->Target);
        this->WorldUp = glm::vec3(0.0f,1.0f,0.0f);
    }

    //////////////////////////////////////////
    // it returns the current view matrix
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(this->Position,this->Target, this->WorldUp);
    }

    //////////////////////////////////////////
    // it updates camera position when a WASD key is pressed
    void ProcessKeyboard(Camera_Movement direction, GLfloat deltaTime)
    {
        GLfloat velocity = this->MovementSpeed * deltaTime;
        if (direction == FORWARD)
            this->Position += this->Front * velocity;
        if (direction == BACKWARD)
            this->Position -= this->Front * velocity;
        if (direction == LEFT)
            ProcessMouseMovement(velocity*100.0f,0.0f);
        if (direction == RIGHT)
            ProcessMouseMovement(-velocity*100.0f,0.0f);
        this->DistanceXZ = DistanceV2(this->Position,this->Target);
    }
    void ChangeTarget(glm::vec3 newTarget)
    {
        this->Target=newTarget;
        updateCameraVectors();
    }


    //////////////////////////////////////////
    // it updates camera orientation when mouse is moved
    //yoffset is my z, roll
    void ProcessMouseMovement(GLfloat xoffset, GLfloat yoffset)
    {
        // the sensitivity is applied to weight the movement
        xoffset *= this->MouseSensitivity;
        yoffset *= this->MouseSensitivity;
        // rotation angles on Z and X
        this->Yaw  += xoffset;
        this->Roll += yoffset;

        if(this->Roll<0.0f)
            this->Roll=0.0f;
        if(this->Roll > 89.0f)
            this->Roll=89.0f;

        updateCameraVectors();
    }
private:
    //distance between 2 vectors
    GLfloat DistanceV2(glm::vec3 a, glm::vec3 b)
    {
        return sqrtf( powf(a.x-b.x,2.0f)+powf(a.z-b.z,2.0f));
    }

    void updateCameraVectors()
    {
        this->Front = glm::normalize(this->Target - this->Position);

        this->Position.x=cos(glm::radians(this->Yaw))*DistanceXZ;
        this->Position.y=sin(glm::radians(this->Roll))*DistanceXZ;
        this->Position.z=sin(glm::radians(this->Yaw))*DistanceXZ;
        this->Position += this->Target;
    }
};

#include "EightBall.h"
using namespace std;

EightBall::EightBall(float table_length, float radius,float scale,glm::vec3 left, glm::vec3 down,bool mode): 
	half_length{table_length*scale*0.5f}, //game's plane half length, original length is 4, width is 2
	radius{radius*scale},
	 left{left}, down{down}, right{-1.0f*left}, force{0.0f},
	 testMode{mode}
	{
		foot_spot=glm::vec3(-half_length/2.0f,radius,0.0f);
		head_spot=glm::vec3(half_length/2.0f,radius,0.0f);
		m_random=mt19937(random_device{}());
		
		//balls=new Ball[BALL_NUM+1];//all color balls + white
		//the white ball property, position doesn't change,always at balls[BALL_NUM]
		balls[BALL_NUM].colorIndex=BALL_NUM;
		balls[BALL_NUM].startPosition=head_spot;
		//set other colored balls
		setInitialPosition();
		shuffle();
		//printBalls();
	}
//position balls at start of game, the balls are positioned with their order
//so they need also be shuffled later
void EightBall::setInitialPosition()
{
	glm::vec3 center=foot_spot; //center point of each row
	//const float sin_a=sqrtf(3.0f)/2.0f; //precalculate the value, sin(angle between ball on left-center and center-top)
	const float sin_a=sinf(_Pi/3.0);
	int i=0;
	float startInc=0.0f;
	glm::vec3 start; //start row position to fill ball

	for(int r=1;r<=5;r++)//r, balls number on each row
	{
		//distance from center    /  if even is radius  |   if odd is 2*radius   |
		//of first 2 balls        /                    o|o,                     ooo
		if(r%2==0)
			startInc=radius;
		else //set ball's position at center, then the rest
		{
			balls[i].startPosition=center;
			balls[i].colorIndex=i++;//set colorIndex as it index
			startInc=2.0f*radius;
		}
		float r2=2.0f*radius;//diameter, distance from each adjacent ball
		//fill from left to limit on left, and right to limito on right
		//i=fillTo(r2*right,center+right*startInc, fillTo(r2*left,center+left*startInc, i, i+r/2), i+r);
		i=fillTo(r2*left,center+left*startInc, i, i+r/2); //fill balls on left
		i=fillTo(r2*right,center+right*startInc, i, i+r/2); //fill balls on right
		/*
			beta
			|\
		   l| \c          l=c*sin(alpha), alpha=60°, because beta=(360°/6)/2
			|__\          I have precalculated sin(PI/3)=sqrt(3)/2, and c=2*radius
				alpha
		*/
		center+=sin_a*r2*down;
	}
}
/*shuffle balls, should be random except for 2 case:
  -on back corner balls one must be a stripe and the other a solid
  -black must be at central position

//the basic idea is change colorIndex and keep same startPosition 
*/
void EightBall::shuffle()
{
	/* balls initial position and they colorindex
				0
			  1   2
			3   4   5
		  6   7   8   9
	   10  11  12   13  14
	*/

	//2 back corner balls, index position 10 and 14, one must be a stripe and the other a solid
	//first assign a random striped color to right corner (14)
	int striped=BLACK_BALL+m_random()%(BLACK_BALL-1);
	//I can swap directly (without temp variable) because value is index, index is value
	balls[BALL_NUM-1].colorIndex=striped; //stripe color, so from BLACK_BALL to BALL_NUM remain stripe color
	balls[striped].colorIndex=BALL_NUM-1;
	//for solid color on opposite corner, I handle after shuffled the remain balls

	//black must at position 4, so index 3
	//I still can directly swap, previous swap do not influence index above (smaller than) BLACK_BALL
	balls[3].colorIndex=BLACK_BALL-1;
	balls[BLACK_BALL-1].colorIndex=3;

	int i=0;
	//first shuffle until black ball index
	for(;i<3;i++)
	{
		int j=i+m_random()%(BALL_NUM-i-1); //random num from i to BALL_NUM-1, avoid index 14 
		
		//avoid j=3, black ball
		while(j==3) j=i+m_random()%(BALL_NUM-i-1);
	   swapColor(i,j);
	}
	//then the rest numbers, except last
	for(++i;i<BALL_NUM-1;i++)
		swapColor(i,i+m_random()%(BALL_NUM-i-1));

	//corner solid color, index at 10
	int corner=10;
	i=0;
	while(balls[corner].colorIndex>BLACK_BALL) //if not solid color
	{
		if(balls[i].colorIndex<BLACK_BALL-1)//find one (no black) and swap
			swapColor(corner,i);
		i++;
	}
	if(m_random()%2==0)//half chance to swap solid and stripe corner ball
		swapColor(corner,BALL_NUM-1);
}
//print color index at "spawed" order, in order as render shows  
void EightBall::printBalls()
{
	int i=0;
	for(int r=1;r<=5;r++)//r balls=each row
	{
		int half=r/2; //balls number from center
		if(r%2==0) //dont count mid ball, when there is not
			half--;

		for(int j=half+i;j>=i;j--) //from center to left
			std::cout<<balls[j].colorIndex<<" ";
		for(int j=half+i+1;j<r+i;j++) //to right
			std::cout<<balls[j].colorIndex<<" ";
		i+=r,
		std::cout<<std::endl;
	}
}
//return force and reset to zero
float EightBall::getForce()
{
	if(testMode)//test mode don't clear force
		return force;
	float tmp=force;
	force=0.0f;
	return tmp;
}
//return force only
float EightBall::_getForce()
{
	return force;
}
//set force value, for test
void EightBall::setForce(float f)
{
	if(testMode)
		force=f;
	else
		cout<<"not in test mode"<<endl;
}
// accumulating force
void EightBall::hitting(float deltaTime)
{
	if(!testMode)//test mode don't accumulate
		force+=deltaTime*f_accumulation;
}

//calculate position for balls[index] to balls[n-1] and assign colorIndex as its index
//the position start at startPos, each loop increase inc
//return index reached = n-1
int EightBall::fillTo(glm::vec3 inc, glm::vec3 startPos,int index, int n)
{
	for(;index<n;index++)
	{
		balls[index].startPosition=startPos;
		balls[index].colorIndex=index;
		startPos+=inc;
	}
	return index;
}
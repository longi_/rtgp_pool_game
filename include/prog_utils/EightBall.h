/*
EightBall class: 
    store balls information (position,color,..) and manage the game rule
    set up the start game for simplification white ball always start at head or foot spot. 

    to add: players, game's point, game constrain and winning condition
*/

#pragma once
#include <iostream>
#include <random>
#include <glm/glm.hpp>
#include <bullet/btBulletDynamicsCommon.h>

//total colored (no white) balls number
//total balls are 15+1, I prefer initialize as 15 because most of position calculation involve colored balls
//and I can use BALL_NUM as index for white ball
const int BALL_NUM=15;
//start index of stride ball, or num of solid ball,(except white)
const int BLACK_BALL=8;
//accumulation of force
const float f_accumulation=20.0f;

//basic ball's information
struct Ball{
    // where locate at beginning
    glm::vec3 startPosition;
    //colorIndex is index at ballColors, colorIndex+1 is ball number
    int colorIndex;
    //is ball in pockets
    bool pocketed;
    btRigidBody* body;
};

//all ball colors, last is white
const glm::vec3 ballColors[]{
                    //full color
    glm::vec3(1.0f,0.98f,0.0f),//yellow, gold
    glm::vec3(0.0f,0.0f,1.0f),//blue
    glm::vec3(1.0f,0.0f,0.0f),//red
    glm::vec3(0.5f,0.0f,0.5f),//purple
    glm::vec3(1.0f,0.27f,0.0f),//orange
    glm::vec3(0.13f,0.54f,0.13f),//green,  forest-green
    glm::vec3(0.5f,0.0f,0.0f),//maroon
    glm::vec3(0.0f,0.0f,0.0f),        //black
                        //double colors:
    glm::vec3(1.0f,0.98f,0.0f), //yellow, gold
    glm::vec3(0.0f,0.0f,1.0f),//blue
    glm::vec3(1.0f,0.0f,0.0f),//red
    glm::vec3(0.5f,0.0f,0.5f),//purple
    glm::vec3(1.0f,0.27f,0.0f),//orange
    glm::vec3(0.13f,0.54f,0.13f),//green, forest-green
    glm::vec3(0.5f,0.0f,0.0f),//maroon
    glm::vec3(1.0f,1.0f,1.0f)         //white
    };
class EightBall
{
    public:
        const float radius;               //ball radius
        bool testMode; //test on force, if is true can set force at will 
        float force;  //force to hit ball
        //left,right,down on prospective from bottom of triangular rack, where ball are placed
        glm::vec3 left,right,down;
        Ball balls[BALL_NUM+1];//all color balls + white
        EightBall(float table_length, float radius,float scale,glm::vec3 left, glm::vec3 down,bool mode);
        ~EightBall()
        {}

        //place balls at start of game and initialize indexcolor
        // the balls are positioned with their index order, so they need be shuffle later
        void setInitialPosition();
        //when player start accumulate force to hit ball
        void hitting(float deltaTime);
        //return force and reset to zero, but dont reset if test mode,
        float getForce();
        //return force only, for test in imGui
        float _getForce();
        //set force value, for test
        void setForce(float f);
    private:
        glm::vec3 foot_spot; //center point on bottom zone
        glm::vec3 head_spot; //center point on top zone
        //half plane length, width is not interested because z=0 is center
        const float half_length; 
		std::mt19937 m_random;

        //shuffle balls, should be random except for 2 case:
        //  -on back corner balls one must be a stripe and the other a solid
        //  -black must be at central position
        void shuffle();
        //swap value colorIndex of struct array balls
        inline void swapColor(int i, int j)
        {
            int tmp=balls[i].colorIndex;
            balls[i].colorIndex=balls[j].colorIndex;
            balls[j].colorIndex=tmp;
        }

        //calculate position for balls[index] to balls[n-1]  and assign colorIndex as its index
        //the position start at startPos, each loop increase inc
        //return index reached = n-1
        int fillTo(glm::vec3 inc, glm::vec3 startPos,int index, int n);
        //print color index at "spawed" order
        void printBalls();
};
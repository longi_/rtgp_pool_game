/*
The project simulate the pool game with rule of 8-ball https://en.wikipedia.org/wiki/Eight-ball
-There are 16 balls (1 black, 1 white, 7 solid color and 7 striped color)
-The table has 6 pockets as described:
https://en.wikipedia.org/wiki/Pool_(cue_sports)#Equipment
https://wpapool.com/equipment-specifications/
the table should be rectangular , size is not fixed but the length is always double than width, so I choose the value of 4x2 units in the model; 
the pockets mouth have different size on corner and side, all are large than twice ball's diameter, so I use that value for all pockets
-The goal is legally pocket the black ball in pocket, which can be done only by player that has cleared his color group balls.
*/
/*
Working progress:
- simple camera movements, that look always on a target
- combine illuminations shader with shadowmap shader
- creation of dynamics world, with customized table shape
- creation of ImGui for test
*/
/*
Command:
-Use muser's cursor to rotate camera, key W,S move camera toward/backward target
-Press space to accumulate force,and release to hit, force direction is camera direction
-Use key C to set cursor's visibility
-The key O,P is used to set orthogonal or perspective view, in the light space matrix for creation of depthmap
-UHJK keys to move light position in Y,Z axis, press one of O,P key to activate change for depthmap
*/

/*
OpenGL coordinate system (right-handed)
positive X axis points right
positive Y axis points up
positive Z axis points "outside" the screen


                              Y
                              |
                              |
                              |________X
                             /
                            /
                           /
                          Z
*/

// Std. Includes
#include <string>

// Loader estensioni OpenGL
// http://glad.dav1d.de/
// THIS IS OPTIONAL AND NOT REQUIRED, ONLY USE THIS IF YOU DON'T WANT GLAD TO INCLUDE windows.h
// GLAD will include windows.h for APIENTRY if it was not previously defined.
// Make sure you have the correct definition for APIENTRY for platforms which define _WIN32 but don't use __stdcall
#ifdef _WIN32
    #define APIENTRY __stdcall
#endif

#include <glad/glad.h>

// GLFW library to create window and to manage I/O
#include <glfw/glfw3.h>

// another check related to OpenGL loader
// confirm that GLAD didn't include windows.h
#ifdef _WINDOWS_
    #error windows.h was included!
#endif
//my game handle class
#include "prog_utils/EightBall.h"
#include <prog_utils/camera_lookingTarget.h>
#include <prog_utils/physics_v2.h>

// classes developed during lab lectures to manage shaders, to load models, for FPS camera, and for physical simulation
#include <utils/shader.h>
#include <utils/model.h>

// we load the GLM classes used in the application
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>

//load dear ImGUI
#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

// we include the library for images loading
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image/stb_image.h>

// dimensions of application's window
GLuint screenWidth = 1200, screenHeight = 900;

// callback functions for keyboard and mouse events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
// if one of the WASD keys is pressed, we call the corresponding method of the Camera class
void apply_camera_movements();

// index of the current shader subroutine (1=BlinnPhong)
GLuint current_subroutine = 1;
// a vector for all the shader subroutines names used and swapped in the application
vector<std::string> shaders;

// the name of the subroutines are searched in the shaders, and placed in the shaders vector (to allow shaders swapping)
void SetupShader(int shader_program);

// print on console the name of current shader subroutine
void PrintCurrentShader(int subroutine);

//create a window to test the parameters
void showGuiMenu(btRigidBody* ball,btRigidBody* plane, btRigidBody* corner);
//generate the framerbuffer and attach to texture for depthmap
void setupShadowmap(const GLuint sWidth, const GLuint sHeight, GLuint *depthMapFBO, GLuint *depthMap);
//render objects model in the scene based on step type 
void renderScene(Shader &shader, Model &table_plan, Model &table_corner, Model &sphere, GLfloat *modelMatrix, int type, bool showLight=false);
//print in stdout a vector3, start with info_start and end with info_end
void printVec3(const glm::vec3 &v, const string &info_start, const string &info_end=" ");
//update camera looking target, based on some criterion
void updateCameraTarget(btRigidBody *body);

//rendering step type, for shadowmap or render normally
enum render_type{SHADOW, RENDER};
// we initialize an array of booleans for each keyboard key
bool keys[1024];

// we need to store the previous mouse position to calculate the offset with the current frame
GLfloat lastX, lastY;

// when rendering the first frame, we do not have a "previous state" for the mouse, so we need to manage this situation
bool firstMouse = true;

// parameters for time calculation (for animations)
GLfloat deltaTime = 0.0f;
GLfloat lastFrame = 0.0f;

// boolean to activate/deactivate wireframe rendering
GLboolean wireframe = GL_FALSE;

// view and projection matrices (global because we need to use them in the keyboard callback)
glm::mat4 view, projection;

// we create a camera. We pass the initial position as a parameter to the constructor. In this case, we use a "floating" camera (we pass false as last parameter)
Camera camera(glm::vec3(15.0f, 15.0f, 0.0f));

// Uniforms to be passed to shaders
// point light position
glm::vec3 lightPos0 = glm::vec3(0.0f, 20.0f, 0.0f);
//more is fov and light hight less is the shadow quality
//must balance light position and fov, minimize both
GLfloat lightFov=95.0f;
glm::mat4 lightProjection, lightView;

//specular,ambient color of balls
GLfloat specularColor[] = {1.0f,1.0f,1.0f};
GLfloat ambientColor[] = {0.1f,0.1f,0.1f};
// weights for the diffusive, specular and ambient components
GLfloat Kd = 0.5f;
GLfloat Ks = 0.4f;
GLfloat Ka = 0.1f;
// shininess coefficient for Phong and Blinn-Phong shaders
GLfloat shininess = 25.0f;

// roughness index for GGX shader
GLfloat alpha = 0.2f;
// Fresnel reflectance at 0 degree (Schlik's approximation)
GLfloat F0 = 0.9f;

// color of the plane
GLfloat planeMaterial[] = {0.0f,0.5f,0.0f};

// instance of the physics class
Physics bulletSimulation;

//scale factor for table and ball
const GLfloat SCALE_VALUE=10.0f;

//table position,scale and balls scale are used in renderScene(), so there are global
//model plane size is 4x2x0.2, plus margin is 4.4x2.4
glm::vec3 table_pos = glm::vec3(0.0f, -0.07f, 0.0f)*SCALE_VALUE;
glm::vec3 table_size = glm::vec3(SCALE_VALUE);
//glm::vec3 table_rot = glm::vec3(0.0f, 0.0f, 0.0f);

//sphere initial radius is 1, to fit the original table, sphere must have r=0.045
glm::vec3 ball_size=glm::vec3(0.045f,0.045f,0.045f)*SCALE_VALUE; 
//glm::vec3 ball_rot = glm::vec3(0.0f, 0.0f, 0.0f);
glm::vec3 start_position=glm::vec3(1.0f,0.045f,0.0f)*SCALE_VALUE;

glm::vec3 lightTarget; //light target, is used to calculate light view
bool cameraTragetChanged=true; //if camera target is changed previously
btRigidBody* white;//white ball collider
GLboolean cursor_visible=false; //flag to set cursors visibility
//create the game mechanics 
EightBall game(4.0f, 0.045f,SCALE_VALUE, //table and ball informations
        glm::vec3(0.0f,0.0f,-1.0f), glm::vec3(-1.0f,0.0f,0.0f),true);//where is left and down

////////////////// MAIN function ///////////////////////
int main(int argc, char* argv[])
{
  // Initialization of OpenGL context using GLFW
  glfwInit();
  // We set OpenGL specifications required for this application
  // In this case: 4.1 Core
  // If not supported by your graphics HW, the context will not be created and the application will close
  // N.B.) creating GLAD code to load extensions, try to take into account the specifications and any extensions you want to use,
  // in relation also to the values indicated in these GLFW commands
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  // we set if the window is resizable
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

  // we create the application's window
   GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "RGP_pool", nullptr, nullptr);
    if (!window)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // we put in relation the window and the callbacks
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    // we disable the mouse cursor
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // GLAD tries to load the context set by GLFW
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress))
    {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        return -1;
    }

    // we define the viewport dimensions
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    // we enable Z test
    glEnable(GL_DEPTH_TEST);

    //the "clear" color for the frame buffer
    glClearColor(0.26f, 0.46f, 0.98f, 1.0f);

    //shader program for shadow map
    Shader shadowmap_shader=Shader("19_shadowmap.vert", "20_shadowmap.frag");
    //Shader quad_shader("debug_quad.vert", "debug_quad_depth.frag"); //shader for depthmap debug

    // the Shader Program for the objects used in the application
    Shader object_shader=Shader("09_illumination_models.vert", "10_illumination_models.frag");
    SetupShader(object_shader.Program);

    // we load the model(s) (code of Model class is in include/utils/model.h)
    Model sphereModel("../../models/sphere.obj");

    Model planeModel("../../models/pool_plane2.obj");//game plane
    Model cornerModel("../../models/pool_table.obj");//corner and pockets

    //create customized collider for table 
    btRigidBody* corner = bulletSimulation.createRigidBodyFromShape(bulletSimulation.createShape(&cornerModel, table_size),table_pos,glm::vec3(0.0f),0.0f,0.1f,0.4f);
    btRigidBody* plane = bulletSimulation.createRigidBodyFromShape(bulletSimulation.createShape(&planeModel, table_size),table_pos,glm::vec3(0.0f),0.0f,0.2f,0.01f);
    
    //create collider for balls, with their start position
    for(Ball& b: game.balls)
        b.body=bulletSimulation.createRigidBody(SPHERE,b.startPosition,ball_size,glm::vec3(0.0f),1.0f,0.1f,1.0f);

    //now I have white ball, change the default target
    camera.ChangeTarget(game.balls[BALL_NUM].startPosition);
    //get address of white ball collider 
    white=game.balls[BALL_NUM].body;

    // we set the maximum delta time for the update of the physical simulation
    GLfloat maxSecPerFrame = 1.0f / 60.0f;
    //set light view direction, perspective light should look down
    lightTarget=glm::vec3(lightPos0.x,0.0f,lightPos0.z);
    //get default light projection and view
    lightProjection = glm::perspective(glm::radians(lightFov), 1.0f, 1.0f, 50.0f);
    lightView = glm::lookAt(lightPos0, glm::vec3(lightPos0.x,0.0f,lightPos0.z), glm::vec3(1.0f, 0.0f, 0.0f));

    //setup shadow map 
    GLuint depthMapFBO,depthMap;
    const GLuint SHADOW_WIDTH = 1024, SHADOW_HEIGHT = 1024;
    setupShadowmap(SHADOW_WIDTH,SHADOW_HEIGHT,&depthMapFBO,&depthMap);

    //get camera projection 
    projection = glm::perspective(45.0f, (float)screenWidth/(float)screenHeight, 0.1f, 10000.0f);
    //default is test mode
    game.testMode=true;
    //initialize imgui
    ImGui::CreateContext();
    if(!ImGui_ImplGlfw_InitForOpenGL(window,true))
        return 1;
    if(!ImGui_ImplOpenGL3_Init("#version 410 core"))
        return 2;

    // Rendering loop: this code is executed at each frame
    while(!glfwWindowShouldClose(window))
    {
        // we determine the time passed from the beginning
        // and we calculate time difference between current frame rendering and the previous one
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Check is an I/O event is happening
        glfwPollEvents();
        // we apply FPS camera movements
        apply_camera_movements();
        

        // we set the rendering mode
        if (wireframe)
            // Draw in wireframe
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        else
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //create gui window
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();
        showGuiMenu(white, plane, corner);

        // we update the physics simulation. We must pass the deltatime to be used for the update of the physical state of the scene.
        // Bullet works with a default timestep of 60 Hz (1/60 seconds). For smaller timesteps (i.e., if the current frame is computed faster than 1/60 seconds), Bullet applies interpolation rather than actual simulation.
        // In this example, we use deltatime from the last rendering: if it is < 1\60 sec, then we use it (thus "forcing" Bullet to apply simulation rather than interpolation), otherwise we use the default timestep (1/60 seconds) we have set above
        // We also set the max number of substeps to consider for the simulation (=10)
        // The "correct" values to set up the timestep depends on the characteristics and complexity of the physical simulation, the amount of time spent for the other computations (e.g., complex shading), etc.
        // For example, this example, with limited lighting, simple materials, no texturing, works correctly even setting:
        // bulletSimulation.dynamicsWorld->stepSimulation(1.0/60.0,10);
        bulletSimulation.dynamicsWorld->stepSimulation((deltaTime < maxSecPerFrame ? deltaTime : maxSecPerFrame),10);
        
        GLfloat matrix[16*(BALL_NUM+1)];
        btTransform transform;
        //get all balls transform location first because I need render twice, for shadow and camera
        for(int i=0;i<=BALL_NUM;i++)
        {
            // we take the transformation matrix of the rigid body, as calculated by the physics engine
            game.balls[i].body->getMotionState()->getWorldTransform(transform);
            // we convert the Bullet matrix (transform) to an array of floats
            transform.getOpenGLMatrix(matrix+i*16);
        }

         /////////////////////////////////// SHADOW MAP STEP
        /// We "install" the  Shader Program for the shadow mapping creation
        shadowmap_shader.Use();
        // we set the viewport for the first rendering step = dimensions of the depth texture
        glViewport(0, 0, SHADOW_WIDTH, SHADOW_HEIGHT);
        // we activate the FBO for the depth map rendering
    glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
        glClear(GL_DEPTH_BUFFER_BIT);
        
        // render from light view space, so calculate transformation matrix for the light
        glm::mat4 lightSpaceMatrix = lightProjection * lightView;

         // we pass the transformation matrix as uniform
        glUniformMatrix4fv(glGetUniformLocation(shadowmap_shader.Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
        //render the scene, using the shadow shader
        renderScene(shadowmap_shader, planeModel,cornerModel,sphereModel,matrix,SHADOW);

        /////////////////////// rendering from CAMERA VIEW STEP
        // we activate back the standard Frame Buffer
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
        // we "clear" the frame and z buffer
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //set the viewport for camera view rendering
        glViewport(0, 0, width, height);
        //install shader for objects
        object_shader.Use();
        // we pass the transformation matrix as uniform
        glUniformMatrix4fv(glGetUniformLocation(object_shader.Program, "lightSpaceMatrix"), 1, GL_FALSE, glm::value_ptr(lightSpaceMatrix));
        //pass the depth map from buffer
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, depthMap);
        GLint shadowLocation = glGetUniformLocation(object_shader.Program, "shadowMap");
        glUniform1i(shadowLocation, 0);

        //update camera target, if camera need follow white or remain old position
        updateCameraTarget(white);
        //render the scene
        renderScene(object_shader, planeModel,cornerModel,sphereModel,matrix,RENDER);

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        // Swapping back and front buffers
        glfwSwapBuffers(window);
    }

    //delete gui
    ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

  // when I exit from the graphics loop, it is because the application is closing
  // we delete the Shader Programs
  object_shader.Delete();
  // we delete the data of the physical simulation
  bulletSimulation.Clear();
  // we close and delete the created context
  glfwTerminate();
  return 0;
}

//////////////////////////////////////////
// If one of the WASD keys is pressed, the camera is moved accordingly (the code is in utils/camera.h)
// press UHJK to move light to see the effect, then is need to press P or O to update
void apply_camera_movements()
{
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
    
    //move light position, for debug use
    if(keys[GLFW_KEY_U]||keys[GLFW_KEY_J]||keys[GLFW_KEY_H]||keys[GLFW_KEY_K])
    {
        if(keys[GLFW_KEY_U])
            lightPos0.y+=0.5f;
        if(keys[GLFW_KEY_J])
            lightPos0.y-=0.5f;
        if(keys[GLFW_KEY_K])
            lightPos0.x+=0.5f;
        if(keys[GLFW_KEY_H])
            lightPos0.x-=0.5f;
        //print data for calculate light view matrix
        printVec3(lightPos0,"lightPos0 ");
        glm::vec3 cameraDirection = glm::normalize(lightPos0 - lightTarget);
        glm::vec3 up=glm::vec3(1.0f,0.0f,0.0f);
        glm::vec3 cameraRight=glm::normalize(glm::cross(cameraDirection,up));
        glm::vec3 cameraUp=glm::cross(cameraDirection,cameraRight);

        printVec3(cameraRight,"right ");
        printVec3(cameraUp,"up ");
        printVec3(cameraDirection,"dir","\n");
    }
}

//////////////////////////////////////////
// callback for keyboard events
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    GLuint new_subroutine;
    // if ESC is pressed, we close the application
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    // if L is pressed, we activate/deactivate wireframe rendering of models
    if(key == GLFW_KEY_L && action == GLFW_PRESS)
        wireframe=!wireframe;

    // if space is pressed and if white ball is not moving, then is allowed to hit
    if(key == GLFW_KEY_SPACE && white->getLinearVelocity().isZero())
    {
        if(action == GLFW_RELEASE)
        {
            //force direction
            glm::vec2 fdir=glm::normalize(glm::vec2(camera.Front.x,camera.Front.z));

            btVector3 impulse=btVector3(fdir.x,0.0f,fdir.y)*game.getForce();
            //impulse=btVector3(force, 1.0f, force);
            std::cout <<" impulse "<<impulse.getX()<<","<< impulse.getY()<<","<< impulse.getZ()<<std::endl;
            white->applyCentralImpulse(impulse);
            white->activate(true); //force activation, some times impulse doesn't work
        }
        else game.hitting(deltaTime);
    }
    //make cursor visible or disabled, default disabled
    if(key==GLFW_KEY_C && action == GLFW_PRESS)
    {
        cursor_visible=!cursor_visible;
        if(cursor_visible)
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
        else
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    }
    // pressing a key number, we change the shader applied to the models
    // if the key is between 1 and 9, we proceed and check if the pressed key corresponds to
    // a valid subroutine
    if((key >= GLFW_KEY_1 && key <= GLFW_KEY_9) && action == GLFW_PRESS)
    {
        // "1" to "9" -> ASCII codes from 49 to 59
        // we subtract 48 (= ASCII CODE of "0") to have integers from 1 to 9
        // we subtract 1 to have indices from 0 to 8
        new_subroutine = (key-'0'-1);
        // if the new index is valid ( = there is a subroutine with that index in the shaders vector),
        // we change the value of the current_subroutine variable
        // NB: we can just check if the new index is in the range between 0 and the size of the shaders vector,
        // avoiding to use the std::find function on the vector
        if (new_subroutine<shaders.size())
        {
            current_subroutine = new_subroutine;
            PrintCurrentShader(current_subroutine);
        }
    }

    //change light projection view matrix to ortho view
    if(key==GLFW_KEY_O && action == GLFW_PRESS)
    {   std::cout<<"ortho ";
        GLfloat near_plane = 1.0f, far_plane = 50.0f;
        GLfloat frustumSize = 10.0f; 
        lightProjection = glm::ortho(-frustumSize, frustumSize, -frustumSize, frustumSize, near_plane, far_plane);
        lightView = glm::lookAt(lightPos0, lightTarget, glm::vec3(0.0f, 1.0f, 0.0f));
    }
    //change light projection view matrix to perspective view
    if(key==GLFW_KEY_P && action == GLFW_PRESS)
    {
        std::cout<<"perspective ";
        lightProjection = glm::perspective(glm::radians(lightFov), 1.0f, 1.0f, 50.0f);
        lightView = glm::lookAt(lightPos0,lightTarget, glm::vec3(1.0f, 0.0f, 0.0f));
    }
    // we keep trace of the pressed keys
    // with this method, we can manage 2 keys pressed at the same time:
    // many I/O managers often consider only 1 key pressed at the time (the first pressed, until it is released)
    // using a boolean array, we can then check and manage all the keys pressed at the same time
    if(action == GLFW_PRESS)
        keys[key] = true;
    else if(action == GLFW_RELEASE)
        keys[key] = false;
}

//////////////////////////////////////////
// callback for mouse events
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
      // we move the camera view following the mouse cursor
      // we calculate the offset of the mouse cursor from the position in the last frame
      // when rendering the first frame, we do not have a "previous state" for the mouse, so we set the previous state equal to the initial values (thus, the offset will be = 0)

    if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    // offset of mouse cursor position
    GLfloat xoffset = xpos - lastX;
    GLfloat yoffset = lastY - ypos;

    // the new position will be the previous one for the next frame
    lastX = xpos;
    lastY = ypos;

    // we pass the offset to the Camera class instance in order to update the rendering
    camera.ProcessMouseMovement(xoffset, yoffset);
}

//////////////////////////////////////////
// we print on console the name of the currently used shader subroutine
void PrintCurrentShader(int subroutine)
{
    std::cout << "Current shader subroutine: " << shaders[subroutine]  << std::endl;
}

///////////////////////////////////////////
// The function parses the content of the Shader Program, searches for the Subroutine type names,
// the subroutines implemented for each type, print the names of the subroutines on the terminal, and add the names of
// the subroutines to the shaders vector, which is used for the shaders swapping
void SetupShader(int program)
{
    int maxSub,maxSubU,countActiveSU;
    GLchar name[256];
    int len, numCompS;

    // global parameters about the Subroutines parameters of the system
    glGetIntegerv(GL_MAX_SUBROUTINES, &maxSub);
    glGetIntegerv(GL_MAX_SUBROUTINE_UNIFORM_LOCATIONS, &maxSubU);
    std::cout << "Max Subroutines:" << maxSub << " - Max Subroutine Uniforms:" << maxSubU << std::endl;

    // get the number of Subroutine uniforms (only for the Fragment shader, due to the nature of the exercise)
    // it is possible to add similar calls also for the Vertex shader
    glGetProgramStageiv(program, GL_FRAGMENT_SHADER, GL_ACTIVE_SUBROUTINE_UNIFORMS, &countActiveSU);

    // print info for every Subroutine uniform
    for (int i = 0; i < countActiveSU; i++) {

        // get the name of the Subroutine uniform (in this example, we have only one)
        glGetActiveSubroutineUniformName(program, GL_FRAGMENT_SHADER, i, 256, &len, name);
        // print index and name of the Subroutine uniform
        std::cout << "Subroutine Uniform: " << i << " - name: " << name << std::endl;

        // get the number of subroutines
        glGetActiveSubroutineUniformiv(program, GL_FRAGMENT_SHADER, i, GL_NUM_COMPATIBLE_SUBROUTINES, &numCompS);

        // get the indices of the active subroutines info and write into the array s
        int *s =  new int[numCompS];
        glGetActiveSubroutineUniformiv(program, GL_FRAGMENT_SHADER, i, GL_COMPATIBLE_SUBROUTINES, s);
        std::cout << "Compatible Subroutines:" << std::endl;

        // for each index, get the name of the subroutines, print info, and save the name in the shaders vector
        for (int j=0; j < numCompS; ++j) {
            glGetActiveSubroutineName(program, GL_FRAGMENT_SHADER, s[j], 256, &len, name);
            std::cout << "\t" << s[j] << " - " << name << "\n";
            shaders.push_back(name);
        }
        std::cout << std::endl;

        delete[] s;
    }
}
//the function create some sub-menu
//each treeNode is an argument type
void showGuiMenu(btRigidBody* ball,btRigidBody* plane, btRigidBody* corner)
{
    //define all tested physics variable
    float p_friction,p_rest,c_friction,c_rest, w_mass,w_friction,w_rest,w_angDump,w_rollFri,w_force;
    bool test_force=true;

    //create window with title
    if(!ImGui::Begin("test window"))
    {
        ImGui::End();
        return;
    }

    //create checkbox of test modo for the force
    ImGui::Checkbox("test force ", &game.testMode);
    ImGui::SameLine();ImGui::PushItemWidth(120);
    //get current force
    w_force=game._getForce();
    //slider for force from 0.0 to 30.0
    ImGui::SliderFloat("force",&w_force,0.0f,30.0f,"%.1f",ImGuiSliderFlags_AlwaysClamp);
    if(game.testMode) //if test mode on, change force
        game.setForce(w_force);

    //create child window for plane physics configuration
    if( ImGui::TreeNode("Table - plane"))
    {
        p_friction=plane->getFriction();
        p_rest=plane->getRestitution();
        ImGui::SliderFloat("friction",&p_friction,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("restitution",&p_rest,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        plane->setFriction(p_friction);
        plane->setRestitution(p_rest);
        ImGui::TreePop();
    }
    //create child window for table corner physics configuration
    if( ImGui::TreeNode("Table - corner"))
    {
        c_friction=corner->getFriction();
        c_rest=corner->getRestitution();
        ImGui::SliderFloat("friction",&c_friction,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("restitution",&c_rest,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        corner->setFriction(c_friction);
        corner->setRestitution(c_rest);
        ImGui::TreePop();
    }
    //create child window for balls physics configuration
    if(ImGui::TreeNode("white ball"))
    {
        w_mass=ball->getMass();
        w_friction=ball->getFriction();
        w_rest=ball->getRestitution();
        w_angDump=ball->getAngularDamping();
        w_rollFri=ball->getRollingFriction();

        ImGui::SliderFloat("mass",&w_mass,0.0f,30.0f,"%.1f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("friction",&w_friction,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("restitution",&w_rest,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("angular dumping",&w_angDump,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        ImGui::SliderFloat("rolling friction",&w_rollFri,0.0f,1.0f,"%.2f",ImGuiSliderFlags_AlwaysClamp);
        
        ball->setMassProps(w_mass,ball->getLocalInertia());
        ball->setFriction(w_friction);
        ball->setRestitution(w_rest);
        ball->setDamping(ball->getLinearDamping(),w_angDump);
        ball->setRollingFriction(w_rollFri);
        
        ImGui::TreePop();
    }
    //create child window for light position and FoV on depth map creation
    //need press 'O' or 'P' to update values
    if(ImGui::TreeNode("light view"))
    {
        ImGui::DragFloat("fov", &lightFov, 1.0f);
        ImGui::Text("light target ");ImGui::SameLine();
        ImGui::PushItemWidth(100);
        ImGui::DragFloat("x", &lightTarget.x, 0.1f);ImGui::SameLine();
        ImGui::DragFloat("y", &lightTarget.y, 0.1f);ImGui::SameLine();
        ImGui::DragFloat("z", &lightTarget.z, 0.1f);
        ImGui::TreePop();
    }
    ImGui::End();
}
//create buffer for depth map with given dimension
//the buffer object and texture id are passed by point
void setupShadowmap(const GLuint sWidth, const GLuint sHeight, GLuint *depthMapFBO, GLuint *depthMap)
{
    // we create a Frame Buffer Object: the first rendering step will render to this buffer, and not to the real frame buffer
    glGenFramebuffers(1, depthMapFBO);
    // we create a texture for the depth map
    glGenTextures(1, depthMap);
    glBindTexture(GL_TEXTURE_2D, *depthMap);
    // in the texture, we will save only the depth data of the fragments. Thus, we specify that we need to render only depth in the first rendering step
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, sWidth, sHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    // we set to clamp the uv coordinates outside [0,1] to the color of the border
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);

    //color outside depthMap's border, 0.0 to show border
    GLfloat borderColor[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

    // we bind the depth map FBO
    glBindFramebuffer(GL_FRAMEBUFFER, *depthMapFBO);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, *depthMap, 0);
    // we set that we are not calculating nor saving color data
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
//render scene objects
//in the shadow mode a lot uniform is ignored, in rendere mode there is activation of subroutine and rendereing light if showLight is true
void renderScene(Shader &shader, Model &table_plan, Model &table_corner, Model &sphere, GLfloat *modelMatrix, int type, bool showLight)
{
    GLuint index; //subroutine index
    //location of the uniform for diffuse color, define outside if because is used for all objects
    GLint objDiffuseLocation = glGetUniformLocation(shader.Program, "diffuseColor");

    /////////////////// STATIC PLANE ////////////////////////////////////////////////
    glm::mat4 tableModelMatrix = glm::mat4(1.0f);
    tableModelMatrix = glm::translate(tableModelMatrix, table_pos);
    tableModelMatrix = glm::scale(tableModelMatrix, table_size);
    glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(tableModelMatrix));

    if(type==RENDER)// if is render install the selected Shader Program as part of the current rendering process
    {
        //get table subroutine index
        index = glGetSubroutineIndex(shader.Program, GL_FRAGMENT_SHADER, "Lambert");
        // we activate the subroutine using the index
        glUniformSubroutinesuiv( GL_FRAGMENT_SHADER, 1, &index);
        //get view matrix
        view=camera.GetViewMatrix();
        // we pass projection and view matrices to the Shader Program
        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "projectionMatrix"), 1, GL_FALSE, glm::value_ptr(projection));
        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "viewMatrix"), 1, GL_FALSE, glm::value_ptr(view));
        // we determine the position in the Shader Program of the uniform variables
        GLint pointLightLocation = glGetUniformLocation(shader.Program, "pointLightPosition");
        GLint kdLocation = glGetUniformLocation(shader.Program, "Kd");
        // we assign the value to the uniform variable
        glUniform3fv(objDiffuseLocation, 1, planeMaterial);
        glUniform3fv(pointLightLocation, 1, glm::value_ptr(lightPos0));
        glUniform1f(kdLocation, Kd);

        glm::mat3 tableNormalMatrix = glm::mat3(1.0f);
        tableNormalMatrix = glm::inverseTranspose(glm::mat3(view*tableModelMatrix));
        glUniformMatrix3fv(glGetUniformLocation(shader.Program, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(tableNormalMatrix));
    }
    //draw table plan and corner with same uniforms
    table_plan.Draw();
    table_corner.Draw();

    if(showLight && type==RENDER) //render light object, a white ball
    {
        glm::mat4 lightModel=glm::mat4(1.0f);
        lightModel=glm::translate(lightModel,lightPos0);
        GLfloat lcolor[]={1.0f,1.0f,1.0f};
        glUniform3fv(objDiffuseLocation, 1, lcolor);
        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(lightModel));
        sphere.Draw();
    }

    /////
    // DYNAMIC OBJECTS, BALLS
    if(type==RENDER)
    {
        //get selected subroutine index
        index = glGetSubroutineIndex(shader.Program, GL_FRAGMENT_SHADER, shaders[current_subroutine].c_str());
        // we activate the subroutine using the index
        glUniformSubroutinesuiv( GL_FRAGMENT_SHADER, 1, &index);
    
        // we determine the position in the Shader Program of the uniform variables
        GLint ambientLocation = glGetUniformLocation(shader.Program, "ambientColor");
        GLint specularLocation = glGetUniformLocation(shader.Program, "specularColor");
        GLint kaLocation = glGetUniformLocation(shader.Program, "Ka");
        GLint ksLocation = glGetUniformLocation(shader.Program, "Ks");
        GLint shineLocation = glGetUniformLocation(shader.Program, "shininess");
        GLint alphaLocation = glGetUniformLocation(shader.Program, "alpha");
        GLint f0Location = glGetUniformLocation(shader.Program, "F0");

        // we assign the value to the uniform variables
        glUniform3fv(ambientLocation, 1, ambientColor);
        glUniform3fv(specularLocation, 1, specularColor);
        glUniform1f(kaLocation, Ka);
        glUniform1f(ksLocation, Ks);
        glUniform1f(shineLocation, shininess);
        glUniform1f(alphaLocation, alpha);
        glUniform1f(f0Location, F0);
    }

    //render balls in forward order
    for(Ball& b: game.balls)
    {
        glUniform3fv(objDiffuseLocation, 1, glm::value_ptr(ballColors[b.colorIndex]));

        // we reset to identity at each frame
        glm::mat4 objModelMatrix = glm::mat4(1.0f);
        glm::mat3 objNormalMatrix = glm::mat3(1.0f);

        objModelMatrix = glm::make_mat4(modelMatrix) * glm::scale(objModelMatrix, ball_size);
        // we create the normal matrix
        objNormalMatrix = glm::inverseTranspose(glm::mat3(view*objModelMatrix));
        glUniformMatrix4fv(glGetUniformLocation(shader.Program, "modelMatrix"), 1, GL_FALSE, glm::value_ptr(objModelMatrix));
        glUniformMatrix3fv(glGetUniformLocation(shader.Program, "normalMatrix"), 1, GL_FALSE, glm::value_ptr(objNormalMatrix));

        sphere.Draw();
        modelMatrix+=16;
    }
}
//change camara target if white ball is not moving
// or follow the ball if is moving at low speed
void updateCameraTarget(btRigidBody *body)
{
    btVector3 v=body->getLinearVelocity();
    //not moving
    if(v.isZero())
    {
        //update target if not changed previously 
        if(!cameraTragetChanged)
        {
            btVector3 position = body->getCenterOfMassPosition();
            std::cout << "change target"<<std::endl;
            camera.ChangeTarget(glm::vec3(position.getX(),position.getY(),position.getZ()));
            cameraTragetChanged=true;
        }
    }
    else //is moving
    {
        // with low speed is acceptable to follow target
        // for accurate calculation should use distance
        if(v.getX()+v.getZ() <= FOLLOW_SPEED)
        {
            btVector3 position = body->getCenterOfMassPosition();
            camera.ChangeTarget(glm::vec3(position.getX(),position.getY(),position.getZ()));
        }
        else //reset the flag 
            cameraTragetChanged=false;
    }
}

void printVec3(const glm::vec3 &v, const string &info_start, const string &info_end)
{
    std::cout<<info_start<<" "<<v.x<<","<<v.y<<" "<<v.z<<info_end;
}